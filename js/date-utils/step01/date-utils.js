var dateUtils = dateUtils || {};
dateUtils.getYear=function(){
    var date = new Date();
    var year = date.getFullYear();
    return year;
}

dateUtils.getDayOfWeek = function(){
    var dayName = ["Sunday","Monday","Tuesday","Wednesday","Thursday","Friday","Saturday"];
    var date = new Date();
    var day = dayName[date.getDay()];
    return day;
}

dateUtils.getMonthOfYear = function(){
    var monthNames = ["January","February","March","April","May","June","July","August","September","October","November","December"];

    var date = new Date();
    var month = monthNames[date.getMonth()];
    return month;
}

dateUtils.getDayOfMonth = function(){
    var date = new Date();
    var day = date.getDate();
    return day;
}

dateUtils.date = function(){
    return dateUtils.getDayOfMonth() + " " +  dateUtils.getDayOfWeek() + " " + dateUtils.getMonthOfYear() + " " + dateUtils.getYear();
}



/**
* Formats a date to look like this: Sunday January 11, 2018
* @method format
*/

dateUtils.format = function(){

    alert(dateUtils.date());
  
}

/**
* Checks to see if a Date object is more than 18 years prior to the current date
* @method isOldEnoughToVote
*
* @param {Date} birthDate		The date to check. 	
* @return {boolean}				True if the date is more than 18 years ago. False if not.
*/
//var birthDate = {month: "January", day: "14", year: 1993};

dateUtils.isOldEnoughToVote = function(birthDate){
    var birthYear = birthDate.year;

    var date = new Date();
    var year = date.getFullYear();

    var yearOfAge = year - 18;
    
    if(birthYear <= yearOfAge){
        alert("You are of age!");
        return true;
    }else{
        alert("you are not of age.");
        return false;
    };
}



