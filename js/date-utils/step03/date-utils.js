var dateUtils = dateUtils || {};


/**
* Checks to see if a Date object is more than 18 years prior to the current date
* @method isOldEnoughToVote
*
* @param {Date} birthDate       The date to check.  
* @return {boolean}             True if the date is more than 18 years ago. False if not.
*/
dateUtils.isOldEnoughToVote = function(birthDate){
    /*
    // Note that this approach works 
    //but may not be accurate because it assumes 365 days in every year
    
    var now = new Date();
    var diffInDays = diff(now, birthDate);
    if( (diffInDays > 18 * 365) && (max(now,birthDate) == now) ){
        return true;
    }else{
        return false;
    }
    */
    
    if((birthDate instanceof Date) == false){
        throw Error("Invalid argument, Date object expected");
    }

    var currentYear = new Date().getFullYear();
    var exactly18yearsAgo = new Date();
    exactly18yearsAgo.setFullYear(currentYear - 18);
    exactly18yearsAgo.setHours(23,59,59,999); // make it the last millisecond of the day 18 years ago
    
    //console.log("BIRTHDAY: ",birthDate, "18 Years ago:",exactly18yearsAgo);
    if(birthDate <= exactly18yearsAgo){
        return true;
    }else{
        return false;
    }
}

/**
* Returns the day name for a given date.
* @method getDayName
* @param {Date} date
* @return {string}
*/
dateUtils.getDayName = function(date){

    if((date instanceof Date) == true){
        var weekdays = new Array(7);
        weekdays[0] = "Sunday";
        weekdays[1] = "Monday";
        weekdays[2] = "Tuesday";
        weekdays[3] = "Wednesday";
        weekdays[4] = "Thursday";
        weekdays[5] = "Friday";
        weekdays[6] = "Saturday";
        return weekdays[date.getDay()];
        
    }else{
        
        throw Error("Invalid argument, Date object expected");
    }

    
}

/**
* Returns the month name for a given date.
* @method getMonthName
* @param {Date} date
* @return {string}
*/
dateUtils.getMonthName = function(date){
   
    if((date instanceof Date) == true){
        var months = new Array(12);
        months[0] = "January";
        months[1] = "February";
        months[2] = "March";
        months[3] = "April";
        months[4] = "May";
        months[5] = "June";
        months[6] = "July";
        months[7] = "August";
        months[8] = "September";
        months[9] = "October";
        months[10] = "November";
        months[11] = "December";
        return months[date.getMonth()];
        
    }else{
        
        throw Error("Invalid argument, Date object expected");
    }
    
}



/**
* Converts milliseconds to days.
* @method convertMillisecondsToDays
*
* @param {number} ms
* @return {number}
*/
dateUtils.convertMillisecondsToDays = function(ms){
    if((typeof ms === 'string') || ms == false){
        throw Error("Invalid argument, Number expected");
    }else{
        ///////////////seconds, minutes, hours, milliseconds
        var milliDay = 60*60*24*1000;
        return ms/milliDay;
    }
    
}


/**
* Returns the latter of two Date objects.
* If both Date objects are storing the exact same time, then the first param is returned.
* @method max
*
* @param {Date} date1
* @param {Date} date2
*
* @return {Date}        Returns the latter of the two date params.
*/
dateUtils.max = function(date1, date2){
    //var param1 = date1.getTime();
    //var param2 = date2.getTime();

    
    //WHY DOES THIS NOT WORK?
    //if((date1 instanceof Date) == true && (date2 instanceof Date) == true){
    //    if(param1 > param2){
    //        return date1;
    //    }else{
    //        return date2;
    //    }
    //}else{
    //    if((date1 instanceof Date) == false || (date2 instanceof Date) == false){
    //        throw Error("Invalid argument, Date object expected");
    //    }        
    //}

    if((date1 && date2 instanceof Date) == true){
        if(date1 > date2){
            return date1;
        }else{
            return date2;
        }
    }else{
        throw Error("Invalid argument, Date object expected");
    }    
}

/**
* Compares two dates and determines the time difference (in days) between them.
* @method diff
*
* @param {Date} date1
* @param {Date} date2
* 
* @return {number}          The number of days between the two dates.
*/
dateUtils.diff = function(date1, date2){
    if((date1 && date2 instanceof Date) == true){
        /////////swap params if negative
        var milliDif = (date1 > date2) 
                            ? Math.abs(date1.getTime() - date2.getTime())
                            :Math.abs(date2.getTime() - date1.getTime());
        var dayDiff = milliDif / (60*60*24*1000);
        var dayDiffRound = Math.floor(dayDiff);
        return dayDiffRound;
    }else{
        throw Error("Invalid argument, Date object expected");
    }    
}

/**
* Formats a date to look like this: Sunday January 11, 2018
* @method format
*
* @param {Date} date
* @return {string}
*/
dateUtils.format = function(date){

    return dateUtils.getDayName(date)+ ", " + dateUtils.getMonthName(date)+ ", " + dateUtils.getDayOfMonth(date) + ", " + dateUtils.getYear(date);

}

