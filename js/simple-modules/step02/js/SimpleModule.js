class SimpleModule{

    constructor(options){
        this.container = options.container || null;
        this.firstName = options.firstName || null;
        this.pressCount = 0;
        this.divFirstName;
        this.btnIncrement;
        this.template = '<div class="simple-module"> \
                            <div class="first-name"></div> \
                            <input  type="button" class="increment" value="Click Me!" /> \
                        </div>';

        this.initialize();    
           
        console.log(options);
    }

    initialize(){
        this.container.innerHTML = this.template;

        this.divFirstName = this.container.querySelector(".first-name");
        this.btnIncrement = this.container.querySelector(".increment");

        this.divFirstName.innerHTML = "Hello " + this.firstName;

        let self = this;

        this.btnIncrement.addEventListener("click", function(){
            self.pressCount++;
            self.update();
        });
    }

    update(){
        this.msg = this.firstName + " you have pressed the button " + this.pressCount + " time(s)!";
        this.divFirstName.innerHTML = this.msg;
        return this.msg;
    }

    setFirstName(newFirstName){
        this.firstName = newFirstName;
    }

}