var namespace = namespace || {};

namespace.FishModule = function(options){

    /////variables////////////////////////////////
    var container = options.container;
	var callback = options.callback;
    var webServiceURL = options.webServiceURL || "//localhost/adv-topics/web-services/fish/";
    var jumboContainer = options.jumboContainer;    

    ////////error handlers////////////////////////
    if(!container){
		//throw new Error("container option is required");
		console.log("container option is required");
	}else if((container instanceof HTMLElement) == false){
		//throw new Error("the container must be an HTMLElement");
		console.log("the container must be an HTMLElement");
    }
    
    // this module depends on the ajax module
	if(!namespace.ajax){
		//throw new Error("this module requires the ajax module");
		console.log("this module requires the ajax module");
    }

    initialize();

    /////////INSTANCE VARIABLES///////////////////

    var inpFishId = document.getElementById("fishId");
    var inpType = document.getElementById("type");
    var inpLength = document.getElementById("length");
    var inpWeight = document.getElementById("weight");
    var inpDescription = document.getElementById("description");
    var btnClear = document.getElementById("btnClear");
    var btnSave = document.getElementById("btnSave");
    var btnDelete = document.getElementById("btnDelete");
    var vType = document.getElementById("vType");
    var vLength = document.getElementById("vLength");
    var vWeight = document.getElementById("vWeight");
    var vDescription = document.getElementById("vDescription");
    var fish = [];

    
    
    //////////FUNCTIONS////////////

    function initialize(){
        jumboContainer.innerHTML = "";
        jumboContainer.innerHTML = `
                                <div class="container">
                                    <div class="jumbotron">
                                    <h1>My Final Project</h1>
                                    <p>Below you find a collection of fish that I have obtained from my fishing career</p>
                                    </div>
                                </div>
                                `;

		container.innerHTML = "";		
        container.innerHTML = `
            <div id="fish-form-container">
                <form id="fish-form" class="fishForm" >
                    <div class="form-group">
                    <label hidden="true">Fish ID</label>
                    <input type="text" id="fishId" readonly="true" value="" hidden="true">
                    </div>
                    <div class="form-group">
                        <label>Type: </label>
                        <input type="text" class="type" id="type" value="" placeholder="Enter Type"/>
                        <span class="validation" id="vType"></span>
                    </div>					                
                    <div class="form-group">
                        <label>Length (inches): </label>
                        <input type="text" id="length" value="" placeholder="Enter Length"/>
                        <span class="validation" id="vLength"></span>
                    </div>
                    <div class="form-group">
                        <label>Weight (lbs): </label>
                        <input type="text" id="weight" value="" placeholder="Enter Weight"/>
                        <span class="validation" id="vWeight"></span>
                    </div>
                    <div class="form-group">
                        <label>Description: </label>
                        <input type="text" id="description" value="" placeholder="Enter Description" class="form-control input-lg"/>
                        <span class="validation" id="vDescription"></span>
                    </div>
                    <div class="form-group">
                        <input type="button" id="btnSave" value="SAVE" class="btn btn-success"/>
                        <input type="button" id="btnClear" value="CLEAR" class="btn btn-secondary"/>
                    </div>
                    <div class="form-group">
                        <input type="button" id="btnDelete" value="DELETE" class="btn btn-danger"/>
                    </div>
				</form>
			</div>
        `;		
        getAllFish();       
    }

    function getAllFish(){

        namespace.ajax.send({
            url: webServiceURL,
            method: "GET",
            callback: function(response){                
                populateFishList(response);
            }
        });
    }	

    function populateFishList(response){
        fish = JSON.parse(response);

        var html = "";
        for(var x = 0; x < fish.length; x++){
            
            html += `<div class="form-group">
                        <li class="list-group-item" fishId="${fish[x].fishId}">                            
                            <h3><u>${fish[x].type}</u></h3>
                            <h4>${fish[x].length} inches </h4>
                            <h4>${fish[x].weight} lbs </h4>
                            <p>${fish[x].description}</p>    
                            <input type="button" class="btn btn-primary" value="EDIT" id="edit"></input>
                        </li>
                    </div>`;
        }
        var ol = document.createElement("ol");
        ol.innerHTML = html;
        container.appendChild(ol);
    }

	function getFishId(id){		

		namespace.ajax.send({
			url: webServiceURL,
			method: "GET",
			callback: function(response){
				fish = JSON.parse(response);
				
				for(var x = 0; x < fish.length; x++){
					if(id == fish[x].fishId){

						let newFish = {
							fishId: fish[x].fishId, 
							type: fish[x].type, 
							length: fish[x].length, 
							weight: fish[x].weight,
							description: fish[x].description
						}
                        populateFishForm(newFish);
					}
                }			
			}
		});
    }        

    function populateFishForm(fish){
        inpFishId.value = fish.fishId;
        inpType.value = fish.type;
        inpLength.value = fish.length;
        inpWeight.value = fish.weight;
        inpDescription.value = fish.description;
    }

    function addFIsh(){
        var newFish = {
            type: inpType.value,
            length:inpLength.value,
            weight: inpWeight.value, 
            description: inpDescription.value
        };

        namespace.ajax.send({
            url: webServiceURL,
            method: "POST",
            requestBody: JSON.stringify(newFish),
            callback: function(response){
                testGetAll();
            }
        });
    }

    function updateFish(){
        var newFish = {
            fishId: inpFishId.value,
            type: inpType.value,
            length:inpLength.value,
            weight: inpWeight.value, 
            description: inpDescription.value
        };

        namespace.ajax.send({
            url: webServiceURL,
            method: "PUT",
            requestBody: JSON.stringify(newFish),
            callback: function(response){              
            }
        });        
    }

    function clearForm(){
        inpFishId.value = "";
        inpType.value = "";
        inpLength.value = "";
        inpWeight.value = "";
        inpDescription.value = "";
        vType.innerHTML = "";
        vLength.innerHTML = "";
        vWeight.innerHTML = "";
        vDescription.innerHTML = "";
    }

    function validateForm(){
        var formIsValid = true;

        if(inpDescription.value == ""){
            vDescription.innerHTML= "Please enter a short description.";
            vDescription.style.display = "inline";
            inpDescription.focus();
            formIsValid = false;
        }

        if(inpWeight.value == ""){
            vWeight.innerHTML = "Please Enter a Weight (Ex: '12.1')";
            vWeight.style.display = "inline";
            inpWeight.focus();
            formIsValid = false;
        }

        if(isNaN(inpWeight.value)){
            vWeight.innerHTML = "Please Enter a number (Ex: '21')";
            vWeight.style.display = "inline";
            inpLength.focus();
            formIsValid = false;
        }

        if(inpLength.value == ""){
            vLength.innerHTML = "Please Enter a Length (Ex: '21')";
            vLength.style.display = "inline";
            inpLength.focus();
            formIsValid = false;
        }

        if(isNaN(inpLength.value)){
            vLength.innerHTML = "Please Enter a number (Ex: '21')";
            vLength.style.display = "inline";
            inpLength.focus();
            formIsValid = false;
        }

        if(inpType.value == ""){
            vType.innerHTML = "Please Enter a Type of Fish (Ex: 'Bass')";
            vType.style.display = "inline";
            inpType.focus();
            formIsValid = false;
        }

        return formIsValid;
    }

    /////////EVENT LISTENERS/////////////////////////

    container.addEventListener("click", function(evt){
		var target = evt.target;

		if(target.classList.contains("btn-primary")){
			var selectedId = target.closest("li").getAttribute("fishId");
            getFishId(selectedId);
            inpType.focus();
		}
    });

    btnClear.addEventListener("click", function(){
        clearForm();
    });

    btnSave.addEventListener("click", function(evt){
        
        evt.preventDefault();
        validateForm();

        if(validateForm()){

            if(inpFishId.value == ""){            
                addFIsh();
                clearForm();
            }else{
                updateFish();   
                getAllFish();   
                clearForm();     
            }  
        }       
    });

    btnDelete.addEventListener("click", function(){
        var deleteFish = {
            fishId: inpFishId.value,
            type: inpType.value,
            length:inpLength.value,
            weight: inpWeight.value, 
            description: inpDescription.value
        };

        //alert(deleteFish.fishId+ "" + webServiceURL);

        namespace.ajax.send({
            url: webServiceURL + deleteFish.fishId,
            method: "DELETE",
            callback: function(response){
            }
        });
    });

    

}