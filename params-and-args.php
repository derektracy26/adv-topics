<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Params and Args</title>
    <script>
    
    function sayHello(firstName, lastName, gender, phoneNumber, email){
        alert("Hello " + firstName + " " + lastName);
    }

    function sayHello2(objParam){
        //if true, set to firstName, otherwise set to empty string .
        var firstName = objParam.firstName || "";
        var lastName = objParam.lastName || "";

        alert("Hello " + firstName + " " + lastName);
    }

    sayHello2({
        firstName: "Bob",
        lastName: "Smith"
    });

    </script>
</head>
<body>
    
</body>
</html>