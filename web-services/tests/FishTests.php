<?php
include_once("../includes/models/Fish.inc.php");


$testResults = array();

testConstructor();
testIsValid();

echo(implode($testResults,"<br>"));

function testConstructor(){
	global $testResults;
	$testResults[] = "<h3>Testing constructor...</h3>";

	// TEST 1 - Make sure we can create a Fish object
	$c = new Fish();
	
	if($c){
		$testResults[] = "PASS - Created instance of Fish model object";
	}else{
		$testResults[] = "FAIL - DID NOT creat instance of a Fish model object";
	}

	// TEST - Make sure the description property gets set correctly
	$options = array(
		'description' => "Betty"
	);

	$c = new Fish($options);

	if($c->description == "Betty"){
		$testResults[] = "PASS - Set description properly";
	}else{
		$testResults[] = "FAIL - DID NOT set description properly";
	}
    
}

function testIsValid(){
	global $testResults;
	$testResults[] = "<h3>Testing isValid()...</h3>";

	// TEST 1 - It should return false if the description is empty
	$c = new Fish(array(
		'description' => "",
		'length' => "14",
		'weight' => "12",
		'type' => "Bass"
	));

	if($c->isValid() === false){
		$testResults[] = "PASS - Validated invalid empty description properly";
	}else{
		$testResults[] = "FAIL - DID NOT validate empty description properly";
	}

	// TEST 2- It should return false if the length is empty
	$c = new Fish(array(
		'description' => "Big Fish",
		'length' => "",
		'weight' => "12",
		'type' => "Bass"
	));

	if($c->isValid() === false){
		$testResults[] = "PASS - Validated invalid empty length properly";
	}else{
		$testResults[] = "FAIL - DID NOT validate empty length properly";
	}

	// TEST 3 - It should return false if the weight is empty
	$c = new Fish(array(
		'description' => "Big Fish",
		'length' => "14",
		'weight' => "",
		'type' => "Bass"
	));

	if($c->isValid() === false){
		$testResults[] = "PASS - Validated empty weight properly";
	}else{
		$testResults[] = "FAIL - DID NOT validate empty weight properly";
	}

	// TEST 4 - It should return false if the type is empty
	$c = new Fish(array(
		'description' => "Big Fish",
		'length' => "14",
		'weight' => "12",
		'type' => ""
	));

	if($c->isValid() === false){
		$testResults[] = "PASS - Validated empty type properly";
	}else{
		$testResults[] = "FAIL - DID NOT empty type properly";
	}


	// TEST 10 - It should return true for the following data
	$c = new Fish(array(
		'description' => "Big Fish",
		'length' => "10",
		'weight' => "10",
		'type' => "Bass"
	));

	if($c->isValid()){
		$testResults[] = "PASS - Validated properly";
	}else{
		$testResults[] = "FAIL - DID NOT validate properly";
	}


}



?>