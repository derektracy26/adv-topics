<?php
include_once('DataAccess.inc.php');
include_once(__DIR__ . "/../models/Fish.inc.php"); 

class FishDataAccess extends DataAccess{


	// CONSTRUCTOR

	function __construct($link){

		parent::__construct($link);
	
	}

	/**
	* 'Cleans' the data in a Fish object to prevent SQL injection attacks
	* @param {Fish}	A Fish model object
	* @return {Fish} A new instance of a Fish object with clean data in it
	*/
	function cleanDataGoingIntoDB($fish){

		if($fish instanceof Fish){
			$cleanFish = new Fish();
			$cleanFish->fishId = mysqli_real_escape_string($this->link, $fish->fishId);
			$cleanFish->type = mysqli_real_escape_string($this->link, $fish->type);
			$cleanFish->length = mysqli_real_escape_string($this->link, $fish->length);
			$cleanFish->weight = mysqli_real_escape_string($this->link, $fish->weight);
			$cleanFish->description = mysqli_real_escape_string($this->link, $fish->description);
			return $cleanFish;
		}else{
			$cleanParam = mysqli_real_escape_string($this->link,$fish);
			return $cleanParam;
		}			
	}
	
	/**
	* 'Cleans' the data in a row from the database (a row should be an associative array)
	* @param {array}	An associative array with key value pairs for each column in the table
	* @return {array} 	A new associative array with clean data in it
	*/
	function cleanDataComingFromDB($row){
		$cleanRow = [];
		$cleanRow['fishId'] = htmlentities($row['fishId']);
		$cleanRow['type'] = htmlentities($row['type']);
		$cleanRow['length'] = htmlentities($row['length']);
		$cleanRow['weight'] = htmlentities($row['weight']);
		$cleanRow['description'] = htmlentities($row['description']);

		return $cleanRow;
	}

	/**
	* Gets all fish from a table in the database
	* @param {assoc array} 	This optional param would allow you to filter the result set
	* 						For example, you could use it to somehow add a WHERE claus to the query
	* 
	* @return {array}		Returns an array of Fish objects
	*/
	function getAll($args = []){
		$qStr = "SELECT fishId, type, length, weight, description FROM fish";
		//die($qStr);

		//MANY PEOPLE RUN QUERIES LIKE THIS, BUT IT COULD SHOW ERROR MESSAGES TO THE USERS
		//$result = mysqli_query($this->link, $qStr) or die(mysqli_error($this->link));

		$result = mysqli_query($this->link, $qStr) or $this->handleError(mysqli_error($this->link));

		$allFish = [];
		if(mysqli_num_rows($result)){
			while($row = mysqli_fetch_assoc($result)){
				$cleanRow = $this->cleanDataComingFromDB($row);
				$fish = new Fish($cleanRow);
				$allFish[] = $fish;
			}
		}
		return $allFish;
	}


	/**
	* Gets a Fish from the database by it's id
	* @param {number} 	The id of the item to get from a row in the database
	* @return {Fish}	 Returns an instance of a Fish model object
	*/
	function getById($id){
		$cleanId = $this->cleanDataGoingIntoDB($id);
		$qStr = "SELECT fishId, type, length, weight, description From fish WHERE fishId = $cleanId";

		$result = mysqli_query($this->link, $qStr) or $this->handleError(mysqli_error($this->link));

		if(mysqli_num_rows($result) == 1){
			$row = mysqli_fetch_assoc($result);
			$cleanRow = $this->cleanDataComingFromDB($row);
			$fish = new Fish($cleanRow);
			return $fish;
		}
		return false;
	}

	
	/**
	* Inserts a fish into a table in the database
	* @param {Fish}	The Fish object to be inserted
	* @return {Fish}	Returns the same Fish object, but with the id property set (the id is assigned by the database)
	*/
	function insert($fish){
		$cleanFish = $this->cleanDataGoingIntoDB($fish);
		$qStr = "INSERT INTO fish (type, length, weight, description) VALUES (
			'{$cleanFish->type}',
			'{$cleanFish->length}',
			'{$cleanFish->weight}',
			'{$cleanFish->description}'
		)";

		$result = mysqli_query($this->link, $qStr) or $this->handleError(mysqli_error($this->link));

		if($result){
			$cleanFish->id = mysqli_insert_id($this->link);
		}else{
			$this->handleError("UNABLE TO INSERT FISH OBJECT");
		}

		return $cleanFish;
	}

	/**
	* Updates a Fish object in the database
	* @param {Fish}	The Fish model object to be updated
	* @return {Fish}	Returns the same Fish model object that was passed in as the param
	*/
	function update($fish){
		$cleanFish = $this->cleanDataGoingIntoDB($fish);
		$qStr = $qStr = "UPDATE fish SET 
			type = '{$cleanFish->type}',
			length = '{$cleanFish->length}',
			weight = '{$cleanFish->weight}',
			description = '{$cleanFish->description}'
		WHERE fishId = {$cleanFish->fishId}";

		$result = mysqli_query($this->link, $qStr) or $this->handleError(mysqli_error($this->link));
		return $cleanFish;
	}


	/**
	* Deletes a Fish object from a table in the database
	* @param {number} 	The id of the Fish to delete
	* @return {boolean}	Returns true if the row was sucessfully deleted, false otherwise
	*/
	function delete($id){
		$cleanId = $this->cleanDataGoingIntoDB($id);
		$qStr = "DELETE FROM fish WHERE fishId = $cleanId";

		$result = mysqli_query($this->link, $qStr) or $this->handleError(mysqli_error($this->link));

		if(mysqli_affected_rows($this->link) == 1){
			return true;
		}
	
		return false;
		
	}
	



} 