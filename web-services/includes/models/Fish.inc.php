<?php
include_once("Model.inc.php");
    
    class Fish extends Model{

        //instance variables
        public $fishId;
        public $type;
        public $length;
        public $weight;
        public $description;

        

        //constructor
        public function __construct($args = []){

            // NOTE that in PHP we use bracket notation for associative arrays
            $this->fishId = $args['fishId'] ?? 0;
            $this->type = $args['type'] ?? "";
            $this->length = $args['length'] ?? "";
            $this->weight = $args['weight'] ?? "";
            $this->description = $args['description'] ?? "";
        }

        //isValid
        public function isValid(){
		    
            //type must not be empty
            if(empty($this->type)){
                return false;
            }
            //description must not be empty
            if(empty($this->description)){
                return false;
            }
            //weight must not be empty
            if(empty($this->weight)){
                return false;
            }   
            //length must not be empty
            if(empty($this->length)){
                return false;
            }           
                      
            return true;
        }
    }
?>